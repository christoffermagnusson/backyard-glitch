﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Button : MonoBehaviour {


	
	
	private Button[] buttonArray;
	
	private Color selected = new Color(1,1,1,1);
	private Color unselected = new Color(0,0,0,1);
	
	private Text costText;
	
	public GameObject defenderPrefab;
	public static GameObject selectedDefender;
	// Use this for initialization
	void Start () {
		
		buttonArray = GameObject.FindObjectsOfType<Button>();
		
		costText = GetComponentInChildren<Text>();
		if(!costText){
			Debug.LogWarning(name+" is not displaying any cost text");
		}
		
		costText.text = defenderPrefab.GetComponent<Defender>().cost.ToString();
	}
	
	
	
	void OnMouseDown(){
		Debug.Log (gameObject+" pressed by player");
		
		foreach(Button thisButton in buttonArray){ // set all buttons to black initially
			thisButton.GetComponent<SpriteRenderer>().color = Color.black;
		}
		
		GetComponent<SpriteRenderer>().color = Color.white; // then set current selected button to white
		selectedDefender = defenderPrefab;
		
	}
}
