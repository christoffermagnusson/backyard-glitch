﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float speed, damage;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.right * speed * Time.deltaTime);
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		GameObject obj = (GameObject) collider.gameObject;
		
		if(obj.GetComponent<Attacker>()){ // this would need tweaking if I introduce Attackers that use Projectiles
			Health health = obj.GetComponent<Health>();
			if(health){
				//Debug.Log(this.ToString()+" hit "+obj.gameObject+" for "+damage+" pts of damage");
				health.DecreaseHealth(damage);
			}
			Destroy(gameObject);
		}
	}
	
	
}
