﻿using UnityEngine;
using System.Collections;

public class LoseCondition : MonoBehaviour {

	public int loseThreshold;
	private LevelManager levelManager;

	void Start(){
		levelManager = FindObjectOfType<LevelManager>();
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		GameObject colliderObj = (GameObject) collider.gameObject;
		
		
		if(colliderObj.GetComponent<Attacker>()){
			loseThreshold--;
			if(loseThreshold<=0){
				Debug.Log ("Game Over!");
				levelManager.LoadLevel("03b Lose");
			}
		}
	}
}
