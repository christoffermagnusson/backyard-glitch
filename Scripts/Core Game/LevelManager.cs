﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
	
	
	public float autoLoadNextLevelAfterSeconds;
	
	private static string lastLoadedLevelName;
	
	
	
	
	
	void Start(){
		if(autoLoadNextLevelAfterSeconds<=0){
			Debug.Log("Level autoload disabled. Use positive count of seconds for loading next level");
		}
		else { // loads start menu after splash screen
			Invoke ("LoadNextLevel",autoLoadNextLevelAfterSeconds);
		}
	}
	
	public void LoadLevel(string name){
		if(name=="02 Level_01" || name=="02 Level_02" || name=="02 Level_03"){
			lastLoadedLevelName = name;
		}
		
		
		Debug.Log ("New Level load: " + name);
		Delay (3);
		Application.LoadLevel (name);
		
		
	}

	public void QuitRequest(){
		Debug.Log ("Quit requested");
		Application.Quit ();
	}
	
	private IEnumerator Delay(int delay){
		yield return new WaitForSeconds(delay);
	}
	
	public void DelayedLoad(string name, int delay){
		Delay (delay);
		LoadLevel (name);
	}
	

	
	public void LoadNextLevel(){
		Application.LoadLevel(Application.loadedLevel +1);
	}
	
	public void LoadNextGameLevel(){
		Debug.Log (lastLoadedLevelName);
		
		if(lastLoadedLevelName=="02 Level_01"){
			LoadLevel("02 Level_02");
		}
		else if(lastLoadedLevelName=="02 Level_02"){
			LoadLevel ("02 Level_03");
		}
		else if(lastLoadedLevelName=="02 Level_03"){
			LoadLevel ("01a Start"); // alternatively Credits! :)
		}
	}
	
	
	
	
	
	
	
	

}
