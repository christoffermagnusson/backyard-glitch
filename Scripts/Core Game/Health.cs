﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public float health;
	
	public void SetHealth(float health){
		this.health = health;
	}
	
	public float GetHealth(){
		return this.health;
	}
	
	public void DecreaseHealth(float damage){
		health -= damage;
			if(health<0){
				// Die animation
				DestroyGameObject();
			}
		}
		
	public void DestroyGameObject(){
		Destroy (gameObject);
	}

	
	public void IncreaseHealth(float newHealth){
		health += newHealth;
	}
}
