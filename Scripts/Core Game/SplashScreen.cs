﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {
	
	private LevelManager levelManager;

	// Use this for initialization
	void Start () {
			levelManager = GameObject.FindObjectOfType<LevelManager>();
			levelManager.DelayedLoad("Start Menu",5);
	}
	
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
