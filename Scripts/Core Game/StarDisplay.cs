﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarDisplay : MonoBehaviour {

	private Text text;
	private int currentStars;
	
	
	public enum Status {SUCCESS, FAILURE};
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		currentStars = int.Parse(text.text);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void AddStars(int newAmount){
		
		currentStars += newAmount;
		
		UpdateDisplay();
	}
	
	public Status UseStars(int cost){
		if(currentStars >= cost){
			currentStars -= cost;
			UpdateDisplay();
			return Status.SUCCESS;
		} 
		return Status.FAILURE;
		
	}
	
	private void UpdateDisplay(){
		text.text = currentStars.ToString();
	}
}
