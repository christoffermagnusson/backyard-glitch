﻿using UnityEngine;
using System.Collections;

public class Attacker : MonoBehaviour {

	private float currentSpeed;
	private GameObject currentTarget;
	
	[Tooltip ("Average time interval this attacker is seen on screen")]
	public float seenEverySeconds;

	
	
	
	
	
	void Start(){
		/*Rigidbody2D myRigidBody2D = gameObject.AddComponent<Rigidbody2D>();
		myRigidBody2D.isKinematic = true;*/
		
		
	}
	
	void Update(){
		
			transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);
			if(!currentTarget){
				GetComponent<Animator>().SetBool("isAttacking",false);
			}
			
			
		
	}
	
	
	
	
	// Sets attacker in attack  mode
	public void Attack(GameObject objToAttack){
		currentTarget = objToAttack;
		
	}
	
	
	// Below methods are called from animator
	public void SetWalkSpeed(float value){
		currentSpeed = value;	
	}
	
	public void StrikeCurrentTarget(float damage){
		//Debug.Log (name+ " attacking for "+damage+ " pts of damage");
		if(currentTarget.GetComponent<Defender>()){
			
			Health health = currentTarget.GetComponent<Health>();
			if(health){
				health.DecreaseHealth(damage);
			}
		}
		
	}
	
	
	
}
