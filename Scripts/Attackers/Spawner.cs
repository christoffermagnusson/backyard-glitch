﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject [] attackerPrefabArray;

	
	
	// Update is called once per frame
	void Update () {
		foreach(GameObject thisAttacker in attackerPrefabArray){
			if(IsTimeToSpawn(thisAttacker)){
				Spawn(thisAttacker);
			}
		}
	}
	
	void Spawn(GameObject attacker){
		GameObject myAttacker = Instantiate (attacker) as GameObject;
		myAttacker.transform.parent = transform;
		myAttacker.transform.position = transform.position;
	}
	
	bool IsTimeToSpawn(GameObject attacker){
		Attacker myAttacker = attacker.GetComponent<Attacker>();
		
		float meanSpawnDelay = myAttacker.seenEverySeconds;
		float spawnsPerSecond = 1 / meanSpawnDelay;
		
		if(Time.deltaTime > meanSpawnDelay){
			Debug.LogWarning ("Spawnrate capped by framerate");
		} 
		
		float threshold = spawnsPerSecond * Time.deltaTime;
		
		return (Random.value < threshold);
			
		
	}
}
