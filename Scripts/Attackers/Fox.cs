﻿using UnityEngine;
using System.Collections;

public class Fox : MonoBehaviour {
	
	private Attacker attacker;
	private Animator animator;
	// Use this for initialization
	void Start () {
		attacker = GameObject.FindObjectOfType<Attacker>();
		animator = GameObject.FindObjectOfType<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		GameObject obj = (GameObject) collider.gameObject;
		
		if(obj.GetComponent<Gravestone>()){
			GetComponent<Animator>().SetTrigger("Jump Trigger");
			
		} else if(obj.GetComponent<Defender>()){
			GetComponent<Animator>().SetBool("isAttacking",true);
			
			attacker.Attack(obj);
		}
	}
}
