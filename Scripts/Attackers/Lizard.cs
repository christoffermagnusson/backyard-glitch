﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Attacker))]
public class Lizard : MonoBehaviour {

	private Attacker attacker;
	
	private Animator animator;

	// Use this for initialization
	void Start () {
		attacker = GetComponent<Attacker>();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void OnTriggerEnter2D(Collider2D collider){
		GameObject obj = collider.gameObject;
		if(obj.GetComponent<Defender>()){
			GetComponent<Animator>().SetBool("isAttacking",true);
			attacker.Attack(obj);
		} else{
			return;
		}
	}
}
