﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameTimer : MonoBehaviour {

	private LevelManager levelManager;

	private Slider gameTimerSlider;
	
	private AudioSource audio;
	private AudioClip winSound;
	private SoundFX soundFX;
	
	private bool hasWon = false; // determines if win condition has been met to control flow of time in update function
	
	private float gameSpeed = (1f/60); // 60 frames a second = 1s / 60 
	public int gameDurationInSeconds;

	// Use this for initialization
	void Start () {
		levelManager = FindObjectOfType<LevelManager>();
		gameTimerSlider = GetComponent<Slider>();
		gameTimerSlider.maxValue = gameDurationInSeconds;
		soundFX = FindObjectOfType<SoundFX>();
		audio = soundFX.GetComponent<AudioSource>();
		winSound = soundFX.soundEffects[0];
		
	}
	
	// Update is called once per frame
	void Update () {
			if(hasWon==false){
				gameTimerSlider.value += gameSpeed;
				}
			if(gameTimerSlider.value==gameTimerSlider.maxValue && hasWon==false){
				hasWon=true;
				Win();	
			}
			
			if(hasWon==true){
				if(!audio.isPlaying){
					Debug.Log ("No audio clip playing!");
					levelManager.LoadLevel("03a Win");
				}
			}
	}
	
	void Win(){
		audio.clip = winSound;
		audio.Play ();
		
		
	}
}
