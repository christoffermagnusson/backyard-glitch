﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {


	public AudioClip[] levelMusicChangeArray;
	
	private AudioSource audio;
	
	
	void Awake(){
		DontDestroyOnLoad(gameObject);
		Debug.Log ("Dont destroy on load: "+name);
	}
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
	}
	
	
	
	public void ChangeVolume(float newVolume){
		audio.volume = newVolume;
	}
	
	void OnLevelWasLoaded(int level){
		AudioClip currentLevelsMusic = levelMusicChangeArray[level];
		
		if(currentLevelsMusic){ // if current level has music attached!
			audio.clip = currentLevelsMusic;
			audio.loop = true;
			audio.Play();
			Debug.Log ("Playing clip: "+currentLevelsMusic);
		}else if(currentLevelsMusic == audio.clip){
			Debug.Log ("Continuing with current track for level: "+level);
		}
		else{
			Debug.LogError("No music attached to level : "+level);
		}
	}
}
