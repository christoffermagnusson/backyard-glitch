﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsController : MonoBehaviour {

	public Slider volumeSlider;
	public Slider difficultySlider;
	public Text difficultyText;
	
	public LevelManager levelManager;
	private MusicManager musicManager;
	
	

	// Use this for initialization
	void Start () {
		musicManager = GameObject.FindObjectOfType<MusicManager>();
		
		volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
		difficultySlider.value = PlayerPrefsManager.GetDifficulty();
	}
	
	// Update is called once per frame
	void Update () {
		musicManager.ChangeVolume(volumeSlider.value);
		// change difficulty accordingly, maybe add a manager for difficulty
		PlayerPrefsManager.SetDifficulty(difficultySlider.value);
		SetDifficultyText(difficultySlider.value);
	}
	
	void SetDifficultyText(float difficulty){
		if(difficulty==1){
			difficultyText.text = "easy";
		} 
		else if(difficulty==2){
			difficultyText.text = "medium";
		}
		else if(difficulty==3){
			difficultyText.text = "hard";
		}
	}
	
	public void SaveAndExit(){
		PlayerPrefsManager.SetMasterVolume(volumeSlider.value);
		PlayerPrefsManager.SetDifficulty(difficultySlider.value);
		levelManager.LoadLevel("01a Start");
	}
	
	public void SetDefaults(){
		volumeSlider.value = 0.7f;
		difficultySlider.value = 2f;
	}
}
