﻿using UnityEngine;
using System.Collections;

public class SetStartVolume : MonoBehaviour {

	private MusicManager musicManager;
	
	// Use this for initialization
	void Start () {
		musicManager = GameObject.FindObjectOfType<MusicManager>();
		float startVolume = PlayerPrefsManager.GetMasterVolume();
		
		musicManager.ChangeVolume(startVolume);
	}
	
	
}
