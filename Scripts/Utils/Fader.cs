﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fader : MonoBehaviour {

	public float fadeInTime; // amount of seconds until fade in is to be completed
	public float fadingSpeed; // the speed at which the Alpha should be reduced per frame
	
	private Image faderImage; // the panel which to be faded
	private Color currentColor = Color.black; // initial coloring of the panel

	// Use this for initialization
	void Start () {
		faderImage = GetComponent(typeof(Image)) as Image;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad < fadeInTime){ // As long as the fadeInTime has not been reached the fader should keep fading
			
			currentColor.a -= Time.deltaTime * fadingSpeed; // reduce the alpha by "fadingSpeed" amount per frame. (if 2 then 120 per second)
			faderImage.color = currentColor; // set the panels color accordingly
		} else{
			gameObject.SetActive(false); // deactivate the panel so interactions can be made
		}
	}
}
