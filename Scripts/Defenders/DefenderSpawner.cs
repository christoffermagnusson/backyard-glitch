﻿using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {

	public Camera myCamera;	
	
	private GameObject defenderParent;
	private StarDisplay starDisplay;
	
	// Use this for initialization
	void Start () {
		starDisplay = FindObjectOfType<StarDisplay>();
		
		defenderParent = GameObject.Find ("Defenders");
		
		if(!defenderParent){
			defenderParent = new GameObject("Defenders");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown(){
		Vector2 spawnPosition = CalculateWorldPointOfMouseClick(Input.mousePosition.x,Input.mousePosition.y);
		
		GameObject defenderToSpawn = Button.selectedDefender; // to check if it is possible to purchase defender
		
		if(IsPurchasePossible(defenderToSpawn)==true){
			SpawnDefender(spawnPosition,defenderToSpawn);
		} else {
			Debug.Log ("Not enough currency to spend!");
		}
		
	}
	
	void SpawnDefender(Vector2 position, GameObject defender){
		
			GameObject newDefender = (GameObject) Instantiate (defender, 
			                                                   position,
			                                                   Quaternion.identity);
			
			newDefender.transform.parent = defenderParent.transform;
			Debug.Log ("Defender spawned at : "+position);
			
	}
	
	private bool IsPurchasePossible(GameObject defenderToCheck){
		
		int defenderCost = defenderToCheck.GetComponent<Defender>().cost; // find the cost of the defender
		if(starDisplay.UseStars(defenderCost)==StarDisplay.Status.SUCCESS){
			return true;
		}
		return false;
	}
	
	Vector2 SnapToGrid(Vector2 rawPos){
		float newPosX = Mathf.RoundToInt(rawPos.x);
		float newPosY = Mathf.RoundToInt(rawPos.y);
		return new Vector2(newPosX,newPosY);
	}
	
	Vector2 CalculateWorldPointOfMouseClick(float x, float y){
		float distanceFromCamera = 10f;
		Vector3 weirdTriplet = new Vector3 (x,y,distanceFromCamera);
		Vector2 worldPos = myCamera.ScreenToWorldPoint(weirdTriplet);
		
		
		return SnapToGrid(worldPos);
	}
}
