﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {


	public GameObject projectile, gun;
	private GameObject projectileParent;
	
	private Animator animator;
	
	private Spawner myLaneSpawner;
	
	void Start(){
		animator = GetComponent<Animator>();
		
		projectileParent = GameObject.Find ("Projectiles");
		
		if(!projectileParent){
			projectileParent = new GameObject("Projectiles");
		}
		
		SetMyLaneSpawner();
			
	}
	
	void Update(){
		if(IsAttackerAheadInLane()){
			animator.SetBool("isAttacking",true);
		} else{
			animator.SetBool("isAttacking",false);
		}
	}
	
	void SetMyLaneSpawner(){
		Spawner [] spawners = FindObjectsOfType<Spawner>();
		
		foreach(Spawner spawner in spawners){
			if(spawner.gameObject.transform.position.y == transform.position.y){
				myLaneSpawner = spawner;
				return;
			}
		}
		Debug.LogError (name+ " can't fins spawner GameObject in lane ");
	}
	
	bool IsAttackerAheadInLane(){
			
		if(myLaneSpawner.gameObject.transform.childCount > 0 ){
					foreach(Transform attacker in myLaneSpawner.transform){
						if(attacker.transform.position.x > transform.position.x){ // check if attackers are behind defenders
							return true;
						}
					}
					
			}
			return false;
		}
		
	
	private void Fire(){
		Vector3 startPosition = gun.transform.position;
		GameObject newProjectile = (GameObject) Instantiate(projectile,
															startPosition,
															Quaternion.identity);
															
		
		newProjectile.transform.parent = projectileParent.transform;
		
	}
	
	
}
