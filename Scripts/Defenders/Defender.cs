﻿using UnityEngine;
using System.Collections;

public class Defender : MonoBehaviour {
	
	private StarDisplay starDisplay;
	
	public int cost;
	
	
	
	void Start(){
		
		starDisplay = FindObjectOfType<StarDisplay>();
		
	}
	
	

	public void AddStars(int amount){
		Debug.Log ("Added "+amount+" stars to currency bank");
		starDisplay.AddStars(amount);
	}
	
	/*public void UseStars(int amount){
		Debug.Log ("Purchase made with "+amount+" stars");
		starDisplay.UseStars(amount);
	}*/
	
	
}
