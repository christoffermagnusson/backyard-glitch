﻿using UnityEngine;
using System.Collections;

public class Gravestone : MonoBehaviour {

	private Defender defender;
	private Animator animator;
	// Use this for initialization
	void Start () {
		defender = GameObject.FindObjectOfType<Defender>();
		animator = GameObject.FindObjectOfType<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerStay2D(Collider2D collider){
		GameObject obj = (GameObject) collider.gameObject;
		
		if(obj.GetComponent<Attacker>()){
			GetComponent<Animator>().SetTrigger("underAttackTrigger");
			// Defender defend?
			// Defender check if Attacker has stopped attacking and then stop defending
		}
	}
	
	
}
